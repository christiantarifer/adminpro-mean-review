const fs = require('fs');

// ************************************* MODELS ************************************* //

const Usuario = require('../models/usuario');
const Medico = require('../models/medico');
const Hospital = require('../models/hospital');

// ********************************************************************************** //

const borrarImagen = ( path ) => {

    if( fs.existsSync( path ) ) {

        // DELETE PREVIOUS IMAGE
        fs.unlinkSync( path )

    }

}

// ********************************************************************************** //
const actualizarImagen = async ( tipo, id, nombreArchivo ) => {

    let pathViejo = ''

    switch( tipo ) {

        case 'medicos':

            try {

                const medico = await Medico.findById( id );

                if( !medico ) {
                    console.error( 'No es un médico en id' );
                    return false;
                }

                pathViejo = `./uploads/medicos/${medico.img}`;
                
                borrarImagen( pathViejo );

                medico.img  = nombreArchivo;

                await medico.save();
                return true;

            } catch (error) {
                console.log(error);
            }
            
        break;

        case 'hospitales':

            try {

                const hospital = await Hospital.findById( id );

                if( !hospital ) {
                    console.error( 'No es un hopital en id' );
                    return false;
                }

                pathViejo = `./uploads/hospitales/${hospital.img}`;
                
                borrarImagen( pathViejo );

                hospital.img  = nombreArchivo;

                await hospital.save();
                return true;

            } catch (error) {
                console.log(error);
            }

        break;

        case 'usuarios':

            try {

                const usuario = await Usuario.findById( id );

                if( !usuario ) {
                    console.error( 'No es un usuario en id' );
                    return false;
                }

                pathViejo = `./uploads/usuarios/${usuario.img}`;
                
                borrarImagen( pathViejo );

                usuario.img  = nombreArchivo;

                await usuario.save();
                return true;

            } catch (error) {
                console.log(error);
            }

        break;

    }
    

}

// ********************************************************************************** //

module.exports = {

    actualizarImagen

}
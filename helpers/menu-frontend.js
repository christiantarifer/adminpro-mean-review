const getMenuFrondEnd = (role = 'USER_ROLE') => {

    const menu = [
        {
          titulo: 'Dashboard',
          icono: 'mdi mdi-gauge',
          submenu: [
            { titulo: 'Main', url: '/' },
            { titulo: 'ProgressBar', url: 'progress' },
            { titulo: 'Gráficas', url: 'charts' },
            { titulo: 'Promesas', url: 'promises' },
            { titulo: 'RXJS', url: 'rxjs' }
          ]
        },
        {
            titulo: 'Mantenimientos',
            icono: 'mdi mdi-folder-lock-open',
            submenu: [
              { titulo: 'Hospitales', url: 'hospitales' },
              { titulo: 'Médicos', url: 'medicos' },
              { titulo: 'Médico', url: 'medico/nuevo' },
            ]
          }
    ];

    if( role === 'ADMIN_ROLE' ) {

        menu[1].submenu.unshift({ titulo: 'Usuarios', url: 'usuarios' })

    }

    return menu;

}

module.exports = {
    getMenuFrondEnd
};
// ************************* THIRD-PARTY PACKAGES ************************** //
require('dotenv').config();

const path = require('path');

const express = require('express');

const cors = require('cors')

// ************************************************************************ //

// ************************* DATABASE ************************** //

const { dbConnection } = require( './database/config' );

// ************************************************************ //

// CREATE EXPRESS SERVER
const app = express();

// CONFIGURE CORS
app.use( cors() )

// READ AND PARSE BODY
app.use( express.json() );

// DATABASE
dbConnection();

// PUBLIC DIR
app.use( express.static('public') );

// ******************************* ROUTES ********************************* //

app.use( '/api/hospitales', require('./routes/hospital.routes') );

app.use( '/api/login', require('./routes/auth.routes' ) );

app.use( '/api/medicos', require('./routes/medico.routes' ) );

app.use( '/api/tests', require('./routes/test.routes' ) );

app.use( '/api/todo', require( './routes/busquedas.routes' ) );

app.use( '/api/upload', require('./routes/uploads.routes' ) );

app.use( '/api/usuarios', require('./routes/usuario.routes' ) );

// ************************************************************************** //

// WILDCARD ROUTE
app.get('*', (req, res) =>{

    res.sendFile( path.resolve( __dirname, 'public/index.html' ) );

});

app.listen( process.env.PORT, () =>  {

    console.log(`Servidor corriendo en ${process.env.PORT}`);

})
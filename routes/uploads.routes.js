/**
 * Path: /api/uploads
 */

// ************************************ THIRD PARTY PACKAGES ************************************ //

const { Router } = require('express');
const expressFileUpload = require('express-fileupload');
const { check } = require('express-validator');

// ********************************************************************************************** //

// ************************************ MIDDLEWARES ************************************ //

const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

// ********************************************************************************************** //

// ************************************ CONTROLLERS ************************************ //

const { fileUpload, retornaImagen } = require('../controllers/uploads.controller');

// ********************************************************************************************** //

const router = Router();

// DEFAULT OPTIONS
router.use(expressFileUpload());

router.put( '/:tipo/:id', validarJWT, fileUpload  );

router.get( '/:tipo/:foto', retornaImagen );

module.exports = router;

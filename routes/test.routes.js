/**
 * Path: /api/test
 */

// ************************************ THIRD PARTY PACKAGES ************************************ //

const { Router } = require('express');

// ********************************************************************************************** //

const router = Router();

router.get('/', ( req, res) => {

    res.status(200).json({
        ok: true,
        msg: 'Hola Mundo'
    })

});

module.exports = router;
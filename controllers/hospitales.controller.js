// ************************************* THIRD PARTY PACKAGES ************************************* // 

const { response } = require('express');


// ************************************************************************************************ //

// ************************************* MODELS ************************************* //

const Hospital = require('../models/hospital');

// ********************************************************************************** //

// ************************************* HELPERS ************************************* // 

const { generarJWT } = require('../helpers/jwt');

// *********************************************************************************** //

// ************************************* FUNCTIONALITY ************************************* //

// -------------------------------------------------------------------------------- //

const getHospitales = async ( req, res = response ) => {

    const desde = Number ( req.query.desde ) || 0;
    const limite = Number( 5 );
    
    const [ hospitales, total ] = await Promise.all([
        Hospital.find()
            .populate('usuario', 'nombre img')
            .skip( desde )
            .limit( limite ),
        Hospital.countDocuments()
    ]);

    res.status(200).json({
        ok: true,
        hospitales,
        total
    })

}

// -------------------------------------------------------------------------------- //

const crearHospital = async ( req, res = response ) => {

    const uid = req.uid;

    const hospital = new Hospital({
        usuario: uid,
        ...req.body
    });

    try {

        const hospitalDB = await hospital.save();
    
        res.status(200).json({
            ok: true,
            hospital: hospitalDB
        })
        
    } catch (error) {
        
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });

    }

    

}

// -------------------------------------------------------------------------------- //

const actualizarHospital = async ( req, res = response ) => {

    const { id } = req.params;
    //const { nombre } = req.body;
    const uid = req.uid;

    try {

        let hospital = await Hospital.findById( id );

        if ( !hospital ) {

            return res.status(404).json({
                ok: false,
                msg: 'Hospital No Existe'
            })

        }

        //hospital.nombre = nombre

        const cambiosHospital = {
            ...req.body,
            usuario: uid
        }

        const hospitalActualizado = await Hospital.findByIdAndUpdate( id, cambiosHospital, { new: true })

        res.status(200).json({
            ok: true,
            hospital: hospitalActualizado
        })

        
    } catch (error) {
        
        res.status(500).json({
            ok: false,
            msg: 'Error en el servidor',
        })


    } 

    
}

// -------------------------------------------------------------------------------- //

const borrarHospital = async ( req, res = response ) => {

    const { id } = req.params;

    try {

        let hospital = await Hospital.findById( id );

        if ( !hospital ) {

            return res.status(404).json({
                ok: false,
                msg: 'Hospital No Existe'
            })

        }

        await Hospital.findByIdAndDelete( id )

        res.status(200).json({
            ok: true,
            msg: 'Hospital Eliminado'
        })
        
    } catch (error) {
        
        res.status(500).json({
            ok: false,
            msg: 'Error en el servidor',
        })

    }

    

}

// -------------------------------------------------------------------------------- //

module.exports = {
    getHospitales,
    crearHospital,
    actualizarHospital,
    borrarHospital
}
// ************************************* THIRD PARTY PACKAGES ************************************* // 

const { response } = require('express');
const bcrypt = require('bcryptjs');

// ************************************************************************************************ // 

// ************************************* MODELS ************************************* // 

const Usuario = require('../models/usuario');

// ********************************************************************************** //

// ************************************* HELPERS ************************************* // 

const { generarJWT } = require('../helpers/jwt');

// *********************************************************************************** //

// ************************************* FUNCTIONALITY ************************************* // 

const getUsuarios =  async ( req, res = response) => {

    const desde = Number ( req.query.desde ) || 0;
    const limite = Number( 5 );

    const [ usuarios, total ] = await Promise.all([
        Usuario
            .find({}, 'nombre email role google img')
            .skip( desde )
            .limit( limite ),
        Usuario.countDocuments()
    ]);


    res.status(200).json({

        ok: true,
        usuarios,
        total

    })

}

// -------------------------------------------------------------------------------- //

const crearUsuarios = async ( req, res = response) => {

    const { email, password } = req.body;

    try {

        const existeEmail = await Usuario.findOne( { email  } );

        if ( existeEmail ) {

            return res.status(400).json({

                ok: false,
                msg: 'El correo ya está registrado'

            })

        }

        const usuario = new Usuario( req.body );

        // * ENCRYPT PASSWORD
        const salt = bcrypt.genSaltSync();

        usuario.password = bcrypt.hashSync( password, salt );

        await usuario.save();

        // * ADD JSON WEB TOKEN
        const token = await generarJWT( usuario.id );
    
        res.status(200).json({
            ok: true,
            usuario,
            token
        })
    
        
    } catch (error) {
        console.log(`%c ${error}`, 'color: #e04e31');
        res.status(500).json({
            ok: false,
            msg: 'Error Inesperado, revisar logs'
        })
    }


}

// -------------------------------------------------------------------------------- //

const actualizarUsuario = async ( req, res = response ) => {

    // TODO: VALIDATE TOKEN AND IF IS THE CORRECT USER

    const uid = req.params.id;

    try {

        const usuarioDB = await Usuario.findById( uid );

        if( !usuarioDB ) {

            return res.status(404).json({
                ok: false,
                msg: 'No existe un usuario por ese id.'
            })

        }

        const { password, google, email, ...campos }= req.body;

        if ( usuarioDB.email !== email ) {

            const existeEmail = await Usuario.findOne({ email });

            if( existeEmail ) {

                return res.status(400).json({
                    ok: false,
                    msg: 'Ya existe un usuario con ese email'
                })

            }

        }

        // * RE ASSING EMAIL SEND IN THE FORM
        if ( !usuarioDB.google ) {
            campos.email = email
        } else if ( usuarioDB.email !== email ) {

            return res.status(400).json({
                ok: false,
                msg: 'Usuarios de google no pueden cambiar su correo.'
            })

        }

        const usuarioActualizado = await Usuario.findByIdAndUpdate( uid, campos, {useFindAndModify: false, new: true} );


        res.status(200).json({
            ok: true,
            usuario: usuarioActualizado
        })

    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado'
        })

    }


}

// -------------------------------------------------------------------------------- //

const borrarUsuario = async ( req, res = response ) => {

    const uid = req.params.id;

    try {
    
        const existeUsuario = await Usuario.findById( uid );

        if( existeUsuario ){

            await Usuario.findByIdAndDelete( uid );

            res.status( 200 ).json({

                ok: true,
                msg: 'Usuario borrado'
        
            })

        } else {

            return res.status(404).json({ 
                ok: false,
                msg: 'El usuario no existe'
            });

        }
        
        

    } catch (error) {
    
        res.status( 500 ).json({

            ok: false,
            msg: 'Error inesperado'
    
        })
        
    }

    

    


}

// ********************************************************************************** //

module.exports = {
    getUsuarios,
    crearUsuarios,
    actualizarUsuario,
    borrarUsuario
}
// ************************************* THIRD PARTY PACKAGES ************************************* // 

const { response } = require('express');


// ************************************************************************************************ //

// ************************************* MODELS ************************************* //

const Hospital = require( '../models/hospital' );
const Medico = require( '../models/medico' );

// ********************************************************************************** //

// ************************************* HELPERS ************************************* // 

const { generarJWT } = require('../helpers/jwt');

// *********************************************************************************** //

// ************************************* FUNCTIONALITY ************************************* //

// -------------------------------------------------------------------------------- //

const getMedicos = async ( req, res = response ) => {

    const desde  = Number( req.query.desde ) || 0;
    const limite = Number( 5 )

    const [medicos, total] =  await Promise.all([
        Medico.find()
            .populate( 'usuario', 'nombre img' )
            .populate( 'hospital', 'nombre img' )
            .skip( desde )
            .limit( limite ),
        Medico.countDocuments()
    ])
    res.status(200).json({
        ok: true,
        medicos,
        total
    })

}

// -------------------------------------------------------------------------------- //

const getMedicoById = async ( req, res = response ) => {

    const {id} = req.params

    const medico = await Medico.findById(id)
                            .populate('usuario', 'nombre img')
                            .populate('hospital', 'nombre img');

    try {

        if( !medico ) {

            return res.status(404).json({
                ok: false,
                msg: 'El id del médico no existe.'
            })
    
        }

        return res.status(200).json({
            ok: true,
            medico
        })
        
    } catch (error) {

        return res.status(500).json({
            ok: false,
            msg: 'Error en el servidor.'
        })
        
    }

}

// -------------------------------------------------------------------------------- //

const crearMedico = async ( req, res = response ) => {

    const uid = req.uid;

    const { nombre, hospital } = req.body;


    try {

        const hospitalDB = await Hospital.findById( hospital );

        if( !hospitalDB ) {

            return res.status(404).json({
                ok: false,
                msg: 'Hospital no existe, por favor revise el identificador.'
            })

        }

        const medico = new Medico({
            nombre,
            usuario: uid,
            hospital
        }) 

        const medicoDB = await medico.save();

        res.status(200).json({
            ok: true,
            medico: medicoDB
        });
        
    } catch (error) {
        
        res.status(500).json({
            ok: true,
            msg: 'Hable con el administrador'
        });

    }
    

}

// -------------------------------------------------------------------------------- //

const actualizarMedico = async ( req, res = response ) => {

    const { id } = req.params;

    const { hospital } = req.body;

    const uid = req.uid;

    try {

        const medicoDB = await Medico.findById(id);

        if( !medicoDB ) {
            return res.status(404).json({
                ok: false,
                msg: 'Medico no encontrado'
            })
        } else {

            const hospitalDB = await Hospital.findById( hospital );

            if ( !hospitalDB ) {

                return res.status(404).json({
                    ok: false,
                    msg: 'Hospital no encontrado'
                })
            }

            const cambioMedico = {
                ...req.body,
                usuario: uid
            }

            const medicoActualizado = await Medico.findByIdAndUpdate( id, cambioMedico, { new: true } )


            res.status(200).json({
                ok: true,
                medico: medicoActualizado
            })

        }

        
        
    } catch (error) {

        
        res.status(500).json({
            ok: false,
            msg: 'Error en el servidor',
        })

    }

    

}

// -------------------------------------------------------------------------------- //

const borrarMedico = async ( req, res = response ) => {

    const { id } = req.params;

    try {


        const medicoDB = await Medico.findById(id);

        if( !medicoDB ) {
            return res.status(404).json({
                ok: false,
                msg: 'Medico no encontrado'
            })
        } else {

            await Medico.findByIdAndDelete( id )

            res.status(200).json({
                ok: true,
                msg: 'Datos del médico han sido borrados satisfactoriamente.'
            })


        }

        
        
    } catch (error) {

        console.log( error );
        
        res.status(500).json({
            ok: false,
            msg: 'Error en el servidor',
        })

    }
    

}

// -------------------------------------------------------------------------------- //

module.exports = {
    getMedicos,
    getMedicoById,
    crearMedico,
    actualizarMedico,
    borrarMedico
}
// getTodo
// ************************************* THIRD PARTY PACKAGES ************************************* // 

const { response } = require('express');

// ************************************************************************************************ //

// ************************************* MODELS ************************************* //

const Medico = require( '../models/medico' )
const Hospital = require('../models/hospital');
const Usuario = require( '../models/usuario' );

// ********************************************************************************** //

// ************************************* HELPERS ************************************* // 

const { generarJWT } = require('../helpers/jwt');

// *********************************************************************************** //

// -------------------------------------------------------------------------------- //

const getTodo = async ( req, res = response ) => {

    const { busqueda } = req.params

    const regexp = new RegExp( busqueda, 'i' )

    const [ usuarios, medicos, hospitales ] = await Promise.all([
        Usuario.find({ nombre: regexp }),
        Medico.find({ nombre: regexp }),
        Hospital.find({ nombre: regexp }),
    ])

    res.status(200).json({
        ok: true,
        usuarios,
        medicos,
        hospitales
    })

}

const getDocumentosColeccion = async ( req, res = response ) => {

    const { tabla, busqueda } = req.params

    const regexp = new RegExp( busqueda, 'i' )

    let data = []

    switch ( tabla ) {

        case 'medicos':
            data = await Medico.find({ nombre: regexp })
                                .populate( 'usuario', 'nombre img' )
                                .populate( 'hospital', 'nombre' )
        break;

        case 'hospitales':
            data = await Hospital.find({ nombre: regexp })
                                 .populate( 'usuario', 'nombre img' )
        break;

        case 'usuarios':
            data = await Usuario.find({ nombre: regexp })
            
        break;
        
        default:
            return res.status(400).json({
                ok: false,
                msg: 'La tabla tiene que ser usuarios/medicos/hospitales'
            })
        
    }

    res.status(200).json({
        ok: true,
        data
    })

}

// -------------------------------------------------------------------------------- //

module.exports = {
    getTodo,
    getDocumentosColeccion
}
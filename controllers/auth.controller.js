// ************************************* THIRD PARTY PACKAGES ************************************* // 

const { response } = require('express');
const bcrypt = require('bcryptjs');

// ************************************************************************************************ //

// ****************************************** MODELS  ********************************************* //

const Usuario = require('../models/usuario');

// ************************************************************************************************ //

// ****************************************** HELPERS  ********************************************* //

const { generarJWT } = require('../helpers/jwt');
const { googleVerify } = require('../helpers/google-verify');
const { getMenuFrondEnd } = require('../helpers/menu-frontend');

// ************************************************************************************************ //

const login = async( req, res = response ) => {

    const { email, password } = req.body;

    try {

        // * VERIFY EMAIL
        const usuarioDB = await Usuario.findOne({ email });

        if ( !usuarioDB ) {

            return res.status(404).json({
                ok: false,
                msg: 'Revisa tus credenciales.'
            })

        }

        // * VERIFY PASSWORD
        const validPassword = bcrypt.compareSync( password, usuarioDB.password );

        if( !validPassword ){

            return res.status(400).json({
                ok: false,
                msg: 'Revisa tus credenciales.'
            })


        }

        // * GENERAR EL TOKEN - JWT
        const token = await generarJWT( usuarioDB.id )

        res.status(200).json({
            ok: true,
            token,
            menu: getMenuFrondEnd(usuarioDB.role)
        })
        
    } catch (error) {
        
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado, por favor comuníquese con el administrador.'
        })

    }

}

const googleSignIn = async ( req, res = response) => {

    const googleToken = req.body.token;

    try {

        const { name, email, picture } = await googleVerify( googleToken );

        const usuarioDB = await Usuario.findOne( { email } )

        let usuario;

        if ( !usuarioDB ) {

            usuario = new Usuario({

                nombre: name,
                email,
                password: '@@@',
                img: picture,
                google: true

            })

        } else {

            usuario = usuarioDB;

            usuario.google = true;

        }

        // SAVE
        await usuario.save();

        // GENERATE JSON
        const token = await generarJWT( usuarioDB.id );


        res.status(200).json({

            ok: true,
            token,
            menu: getMenuFrondEnd(usuario.role)

        });

    } catch ( error ) {

        res.status(401).json({

            ok: false,
            msg: 'Token no es correcto.',
            
        });

    }

    

}

const renewToken = async ( req, res = response ) => {

    const uid = req.uid;

    // GENERATE JSON
    const token = await generarJWT( uid );

    // GETT USER
    const usuario = await Usuario.findById( uid )

    res.status(200).json({
        ok: true,
        token,
        usuario,
        menu: getMenuFrondEnd(usuario.role)
    })

}

module.exports = {
    login,
    googleSignIn,
    renewToken
}
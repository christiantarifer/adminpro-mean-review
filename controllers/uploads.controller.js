const path = require('path');
const fs = require('fs');

// ************************************* THIRD PARTY PACKAGES ************************************* // 

const { response } = require('express');
const { v4: uuidv4 } = require('uuid');

// ************************************************************************************************ //

// ************************************* MODELS ************************************* //

const Medico = require( '../models/medico' )
const Hospital = require('../models/hospital');
const Usuario = require( '../models/usuario' );

// ********************************************************************************** //

// ************************************* HELPERS ************************************* // 

const { generarJWT } = require('../helpers/jwt');
const { actualizarImagen } = require('../helpers/actualizar-helper');

// *********************************************************************************** //

// -------------------------------------------------------------------------------- //

const fileUpload = ( req, res = response ) => {

    const { tipo, id } = req.params;

    // VALIDATE TYPE
    const tiposValidos = [ 'hospitales', 'medicos', 'usuarios' ];

    // VALIDATE TYPE
    if( !tiposValidos.includes( tipo ) ) {

        return res.status(400).json({
            ok: false,
            msg: `No está dentro de: ${tiposValidos} (tipos)` 

        })

    }

    // VALIDATE FILE UPLOADED
    if (!req.files || Object.keys(req.files).length === 0) {

        return res.status(400).json({
            ok: false,
            msg: 'No se cargaron ficheros.'
        });

    }

    // PROCESS IMAGE
    const file = req.files.imagen;

    const nombreCortado = file.name.split( '.')
    const ultimaPosicion =  Number(nombreCortado.length) - 1 

    const extensionArchivo = nombreCortado[ ultimaPosicion ];

    const extensionesValidas = [ 'png', 'jpg', 'jpeg', 'gif' ];

    if( !extensionesValidas.includes( extensionArchivo ) ) {

        return res.status(400).json({
            ok: false,
            msg: 'Extensión no soportada.'
        });

    }

    // GENERATE FILE NAME
    const nombreArchivo = `${ uuidv4() }.${extensionArchivo}`;

    // PATH TO STORAGE IMAGE
    const path = `./uploads/${tipo}/${nombreArchivo}`;

    // MOVE IMAGE
    file.mv( path , ( err ) => {

        if (err) {

            console.log( err );
            return res.status(500).json({
                ok: false,
                msg: 'Error al mover la imagen'
            });

        }

        // UPDATE DATA BASE
        actualizarImagen( tipo, id, nombreArchivo );
        
        res.status(200).json({
            ok: true,
            msg: 'Archivo subido',
            nombreArchivo
        })

    });
    
}

const retornaImagen = ( req, res = response ) => {

    const { tipo, foto } = req.params;

    let pathImg = path.join( __dirname, `../uploads/${tipo}/${foto}` );

    if ( fs.existsSync( pathImg ) ) {

        res.sendFile( pathImg );

    } else {

        pathImg = path.join( __dirname, `../uploads/no-img.jpg` );

        res.sendFile( pathImg )

    }

    
}

// -------------------------------------------------------------------------------- //

module.exports = {
    fileUpload,
    retornaImagen
}